package br.com.cunha.mps.controller.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.cunha.mps.util.RiskEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@Tag(name="ProjectRequest", description = "Informações do Projeto")
@Schema(name = "ProjectRequest", description = "Informações do Projeto")
@AllArgsConstructor
@NoArgsConstructor
public class ProjectRequest {

	@Schema(name = "name", description = "Nome do Projeto", required = true )
	@NotBlank
	@NotNull
	private String name;

	@Schema(name = "startDate", description = "Data inicio do projeto", required = true )
	private LocalDate startDate;

	@Schema(name = "expectedEndDate", description = "Estimativa do encerramento do projeto", required = true )
	private LocalDate expectedEndDate;

	@Schema(name = "endDate", description = "Data do encerramento do projeto", required = true )
	private LocalDate endDate;

	@Schema(name = "description", description = "Descrição do Projeto", required = true )
	private String description;

	@Schema(name = "status", description = "Status do projeto. Caso não seja informado assumirá o valor padrão EM_ANALISE", required = true )
	@Builder.Default
	private StatusProjectEnum status = StatusProjectEnum.EM_ANALISE;

	@Schema(name = "budget", description = "Valor de investimento", required = true )
	private BigDecimal budget;

	@Schema(name = "risk", description = "Classificação tipo de risco", required = true )
	@Builder.Default
	private RiskEnum risk = RiskEnum.BAIXO;

	@Schema(name = "managerId", description = "Id do membro da Equipe com do tipo (F)uncionário. Se informar um membro do tipo diferente de F o registro será recusado", required = true )
	@NotNull(message = "project.request.manager-id")
	private Long managerId;
}
