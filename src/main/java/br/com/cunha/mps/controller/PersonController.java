package br.com.cunha.mps.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cunha.mps.controller.dto.PersonFilterRequest;
import br.com.cunha.mps.controller.dto.PersonRequest;
import br.com.cunha.mps.controller.dto.PersonResponse;
import br.com.cunha.mps.controller.mapper.PersonFilterRequestMapper;
import br.com.cunha.mps.controller.mapper.PersonRequestMapper;
import br.com.cunha.mps.controller.mapper.PersonResponseMapper;
import br.com.cunha.mps.controller.openapi.PersonControllerOpenApi;
import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.domain.service.PersonService;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/person")
@RequiredArgsConstructor
@Slf4j(topic = "PersonController")
public class PersonController implements PersonControllerOpenApi {

	private final PersonRequestMapper personRequestMapper;
	private final PersonFilterRequestMapper personFilterRequestMapper;
	private final PersonResponseMapper personResponseMapper;
	private final PersonService personService;

	@Override
	@PostMapping("/v1/persons" )
	public ResponseEntity<ResponseDTO<PersonResponse>> post( @Valid @RequestBody PersonRequest personRequest ){

		log.info("Inciando o Request para gravar um novo membro");

		PersonDTO personDTO = personRequestMapper.to(personRequest);

		PersonDTO response = personService.save(personDTO);

		log.info( personDTO.toString() );

		PersonResponse personResponse = personResponseMapper.to(response);

		return ResponseEntity
				.status( HttpStatus.CREATED )
				.body( new ResponseDTO<>( personResponse ));
	}

	@Override
	@PutMapping("/v1/persons/{id}" )
	public ResponseEntity<ResponseDTO<PersonResponse>> put( @Valid @RequestBody PersonRequest personRequest ,@PathVariable Long id){

		log.info("Inciando o Request para gravar a alteração dos dados do membro");

		PersonDTO personDTO = personRequestMapper.to(personRequest);
		personDTO.setId(id);
		PersonDTO response = personService.save(personDTO);

		log.info( personDTO.toString() );

		PersonResponse personResponse = personResponseMapper.to(response);

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( personResponse ));
	}


	@Override
	@DeleteMapping("/v1/persons/{id}" )
	public ResponseEntity<ResponseDTO<String>> delete( @PathVariable Long id){
		log.info("Inciando o Request para gravar um novo membro");
		personService.delete(id);
		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( "Removido com sucesso" ));
	}

	@Override
	@GetMapping("/v1/persons/{id}" )
	public ResponseEntity<ResponseDTO<PersonResponse>> findById( @PathVariable Long id){
		PersonDTO response = personService.findById(id);

		PersonResponse personResponse = personResponseMapper.to(response);

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( personResponse ));
	}

	@Override
	@GetMapping("/v1/persons/find-by" )
	public ResponseEntity<ResponseDTO<PageDTO<PersonResponse>>> findBy( PersonFilterRequest personFilterRequest, Integer page, Integer size ){

		PersonFilterDTO personFilterDTO = personFilterRequestMapper.to(personFilterRequest);

		PageDTO<PersonDTO> response = this.personService.findBy(personFilterDTO, page, size);

		List<PersonResponse> personResponses = personResponseMapper.to(response.getContent());

		PageDTO<PersonResponse> pageResponse = new PageDTO<>(personResponses);

		pageResponse.setFirst(response.isFirst());
		pageResponse.setLast(response.isLast());
		pageResponse.setNumber(response.getNumber());
		pageResponse.setNumberOfElements(response.getNumberOfElements());
		pageResponse.setSize(response.getSize());
		pageResponse.setTotalElements(response.getTotalElements());
		pageResponse.setTotalPages(response.getTotalPages());

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( pageResponse ));
	}
}