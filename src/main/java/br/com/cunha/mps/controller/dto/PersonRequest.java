package br.com.cunha.mps.controller.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import br.com.cunha.mps.util.EmployeeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@Tag(name="PersonRequest", description = "Informações do membro utilizados para salvar ou atualizar")
@Schema(name = "PersonRequest", description = "Informações do membro utilizados para salvar ou atualizar")
@AllArgsConstructor
@NoArgsConstructor
public class PersonRequest {

	@Schema(name = "name", description = "Nome do membro da equipe", required = true )
	@NotBlank(message = "person.request.name")
	@NotNull(message = "person.request.name")
	private String name;

	@Schema(name = "dateOfBirth", description = "Data de nascimento da equipe", required = true)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dateOfBirth;

	@Schema(name = "cpf", description = "CPF do membro. Permitido gravar somente um membro por CPF", required = true)
	private String cpf;

	@Schema(name = "employee", description = "Identificador se o membro do Equipe é (F)uncionário ou (P)arceiro", required = true)

	@Builder.Default
	private EmployeeEnum employee = EmployeeEnum.P;
}