package br.com.cunha.mps.controller.dto;

import java.util.Set;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Tag(name = "ManagerRequest" , description = "Membro que serão associados/dessassociados do projeto")
@Schema(name = "ManagerRequest" , description = "Membro que serão associados/dessassociados do projeto")
public class ManagerRequest {
	@NotNull
	@NotEmpty
	@Schema(name = "membersId" , description = "Lista do IDs dos membro que serão associados/dessassociados do projeto")
	private Set<Long> membersId;
}
