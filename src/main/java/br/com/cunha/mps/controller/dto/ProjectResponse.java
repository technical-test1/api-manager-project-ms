package br.com.cunha.mps.controller.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import br.com.cunha.mps.util.RiskEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Tag(name = "ProjectResponse" , description = "Representa as informações do projeto no retorno das apis")
@Schema(name = "ProjectResponse" , description = "Representa as informações do projeto no retorno das apis")
public class ProjectResponse {

	@Schema(name = "id", description = "Código único identificado do projeto")
	private Long id;

	@Schema(name = "name", description = "Nome do Projeto", required = true )
	private String name;

	@Schema(name = "startDate", description = "Data inicio do projeto", required = true )
	private LocalDate startDate;

	@Schema(name = "expectedEndDate", description = "Estimativa do encerramento do projeto", required = true )
	private LocalDate expectedEndDate;

	@Schema(name = "endDate", description = "Data do encerramento do projeto", required = true )
	private LocalDate endDate;

	@Schema(name = "description", description = "Descrição do Projeto", required = true )
	private String description;

	@Schema(name = "status", description = "Status do projeto.", required = true )
	private StatusProjectEnum status;

	@Schema(name = "budget", description = "Valor de investimento", required = true )
	private BigDecimal budget;

	@Schema(name = "risk", description = "Classificação tipo de risco", required = true )
	private RiskEnum risk;

	@Schema(name = " manager", description = "Gerente responsável pelo projeto", required = true )
	private PersonResponse manager;

	@Schema(name = "membros", description = "Membros do projeto", required = true )
	private Set<PersonResponse> members;

}
