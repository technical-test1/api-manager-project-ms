package br.com.cunha.mps.controller.mapper;

import org.mapstruct.Mapper;

import br.com.cunha.mps.controller.dto.ProjectResponse;
import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {PersonResponseMapper.class})
public interface ProjectResponseMapper extends ConverterMapper<ProjectDTO, ProjectResponse>{

}
