package br.com.cunha.mps.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.cunha.mps.controller.dto.PersonRequest;
import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {})
public interface PersonRequestMapper extends ConverterMapper<PersonRequest, PersonDTO>{

	@Mapping(target =  "id" , ignore = true)
	PersonDTO to(PersonRequest source);
}