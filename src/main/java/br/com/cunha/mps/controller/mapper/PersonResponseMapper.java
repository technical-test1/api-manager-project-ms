package br.com.cunha.mps.controller.mapper;

import org.mapstruct.Mapper;

import br.com.cunha.mps.controller.dto.PersonResponse;
import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {})
public interface PersonResponseMapper extends ConverterMapper<PersonDTO, PersonResponse>{

}
