package br.com.cunha.mps.controller.dto;

import java.time.LocalDate;

import br.com.cunha.mps.util.RiskEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Tag(name = "ProjectFilterRequest" , description = "Parametros utilizados para efetuar uma pesquisa por filtros")
@Schema(name = "ProjectFilterRequest" , description = "Parametros utilizados para efetuar uma pesquisa por filtros")
public class ProjectFilterRequest {

	@Schema(name = "name" , description = "Nome do projeto")
	private String name;

	@Schema(name = "manegerId" , description = "Id do gerente associado ao projeto")
	private Long manegerId;

	@Schema(name = "memberId" , description = "Id do membro associado ao projeto")
	private Long memberId;

	@Schema(name = "startDate" , description = "Pesquisa apartir de por data inicial do projeto")
	private LocalDate startDate;

	@Schema(name = "endDate" , description = "Pesquisa até de por data final do projeto")
	private LocalDate endDate;

	@Schema(name = "startExpectedDate" , description = "Pesquisa apartir de por data expectativa de finalização do projeto")
	private LocalDate startExpectedDate;

	@Schema(name = "endExpectedDate" , description = "Pesquisa até de por data expectativa de finalização do projeto")
	private LocalDate endExpectedDate;

	@Schema(name = "risk" , description = "Pesquisa tipo de risco do projeto")
	private RiskEnum risk;

	@Schema(name = "status" , description = "Pesquisa tipo de status do projeto")
	private StatusProjectEnum status;
}
