package br.com.cunha.mps.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cunha.mps.controller.dto.ManagerRequest;
import br.com.cunha.mps.controller.openapi.ManagementControllerOpenApi;
import br.com.cunha.mps.domain.service.ManagementService;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/manager")
@RequiredArgsConstructor
@Slf4j(topic = "ManagementController")
public class ManagementController implements ManagementControllerOpenApi {

	private final ManagementService managementService;

	@Override
	@PostMapping("/v1/manager/associate-members/{projectId}" )
	public ResponseEntity<ResponseDTO<String>> associateMembers( @PathVariable Long projectId, @Valid @RequestBody ManagerRequest managerRequest ){

		log.info("Iniciando o processo de associar os membros no projeto");
		managementService.associateMembers(projectId, managerRequest.getMembersId());

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( "Membros associado com sucesso" ));
	}

	@Override
	@PostMapping("/v1/manager/disassociate-members/{projectId}" )
	public ResponseEntity<ResponseDTO<String>> disassociateMembers( @PathVariable Long projectId, @Valid @RequestBody ManagerRequest managerRequest ){

		managementService.disassociateMembers(projectId, managerRequest.getMembersId());

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( "Membros desassociado com sucesso" ));
	}
}