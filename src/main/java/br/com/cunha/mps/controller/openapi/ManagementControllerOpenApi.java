package br.com.cunha.mps.controller.openapi;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.cunha.mps.controller.dto.ManagerRequest;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import br.com.cunha.mps.infra.dto.ResponseErrorDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "ManagementController", description = "Gerenciamento dos projetos")
public interface ManagementControllerOpenApi {

	@Operation(summary = "Associar Membros", description = "Associar os membros no projeto",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Membro associado com sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<String>> associateMembers(Long projectId, ManagerRequest managerRequest);
	@Operation(summary = "Desassociar Membros", description = "Desassociar os membros no projeto",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Membro desassociado com sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<String>> disassociateMembers(Long projectId, ManagerRequest managerRequest);

}