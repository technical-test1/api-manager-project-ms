package br.com.cunha.mps.controller.openapi;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.cunha.mps.controller.dto.ProjectFilterRequest;
import br.com.cunha.mps.controller.dto.ProjectRequest;
import br.com.cunha.mps.controller.dto.ProjectResponse;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import br.com.cunha.mps.infra.dto.ResponseErrorDTO;
import br.com.cunha.mps.util.StatusProjectEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "ProjectController", description = "Gerencia CRUD dos projetos")
public interface ProjectControllerOpenApi {

	@Operation(summary = "Grava um novo projeto", description = "Responsável em receber as informações do projeto e efetuar a gravação",
			responses = {
					@ApiResponse(
							responseCode = "201",
							description = "Projeto Criado com sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})

	ResponseEntity<ResponseDTO<ProjectResponse>> post(ProjectRequest projectRequest);

	@Operation(summary = "Alterar um projeto", description = "Responsável em receber as informações do projeto e efetuar a alteração",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Projeto alterado com sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<ProjectResponse>> put(Long projectId, ProjectRequest projectRequest);

	@Operation(summary = "Consultar um projeto pelo ID", description = "Responsável retornar as informações do projeto",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Projeto localizado com sucesso" ),
					@ApiResponse(
							responseCode = "404",
							description = "Não foi localizado o projeto na base de dados",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<ProjectResponse>> findById(Long projectId);
	@Operation(summary = "Consultar uma lista dos projetos",
			description = "Responsável retornar uma lista de informações dos projetos pelo filtro especifico",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Projetos localizados com sucesso" ),
					@ApiResponse(
							responseCode = "404",
							description = "Não foi localizado o projeto na base de dados",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<PageDTO<ProjectResponse>>> findBy(ProjectFilterRequest projectFilterRequest,
			Integer page, Integer size);

	@Operation(summary = "Alterar o status do projeto",
			description = "Responsável por efetuar a alteração do status do projeto",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Status do projeto alterado com sucesso" ),
					@ApiResponse(
							responseCode = "404",
							description = "Projeto não localizado para efetuar a alteração do status",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<String>> putUpdateStatus(Long projectId, StatusProjectEnum status);

	@Operation(summary = "Remover um projeto",
			description = "Responsável por apagar um projeto",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Projeto removido com sucesso" ),
					@ApiResponse(
							responseCode = "404",
							description = "Não foi possivel remover o projeto/Projeto não localizado",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<String>> delete(Long projectId);

}