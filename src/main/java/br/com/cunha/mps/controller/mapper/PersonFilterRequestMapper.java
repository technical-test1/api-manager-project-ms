package br.com.cunha.mps.controller.mapper;

import org.mapstruct.Mapper;

import br.com.cunha.mps.controller.dto.PersonFilterRequest;
import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {})
public interface PersonFilterRequestMapper extends ConverterMapper<PersonFilterRequest, PersonFilterDTO>{

}
