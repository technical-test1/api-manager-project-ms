package br.com.cunha.mps.controller.openapi;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.cunha.mps.controller.dto.PersonFilterRequest;
import br.com.cunha.mps.controller.dto.PersonRequest;
import br.com.cunha.mps.controller.dto.PersonResponse;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import br.com.cunha.mps.infra.dto.ResponseErrorDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "PersonController", description = "Gerenciar o CRUD dos membros")
public interface PersonControllerOpenApi {

	@Operation(summary = "Incluir novo membro", description = "Incluir um novo membro",
			responses = {
					@ApiResponse(
							responseCode = "201",
							description = "Inclusão ocorreu com sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<PersonResponse>> post(PersonRequest personRequest);

	@Operation(summary = "Alterar os dados de um Membro", description = "Alterar os dados dos Membros",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Alteração ocorreu com sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<PersonResponse>> put(PersonRequest personRequest, Long id);

	@Operation(summary = "Apagar um membro", description = "Apagar um membro",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Membro removido como sucesso" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<String>> delete(Long id);

	@Operation(summary = "Consultar membro por ID", description = "Consultar membros por ID",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Membro removido como sucesso" ),
					@ApiResponse(
							responseCode = "404",
							description = "Not Found - Quando não contrar o registro na base de dados",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<PersonResponse>> findById(Long id);

	@Operation(summary = "Consultar membro por parametros", description = "Consultar membro por parametros",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Membro removido como sucesso" ),
					@ApiResponse(
							responseCode = "404",
							description = "Not Found - Quando não contrar registros na base de dados",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	ResponseEntity<ResponseDTO<PageDTO<PersonResponse>>> findBy(PersonFilterRequest personFilterRequest, Integer page,
			Integer size);

}