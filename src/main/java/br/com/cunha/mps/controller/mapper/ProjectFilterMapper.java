package br.com.cunha.mps.controller.mapper;

import org.mapstruct.Mapper;

import br.com.cunha.mps.controller.dto.ProjectFilterRequest;
import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {})
public interface ProjectFilterMapper extends ConverterMapper<ProjectFilterRequest, ProjectFilterDTO>{

}
