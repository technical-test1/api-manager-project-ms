package br.com.cunha.mps.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.cunha.mps.controller.dto.ProjectRequest;
import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {})
public abstract class ProjectRequestMapper implements ConverterMapper<ProjectRequest, ProjectDTO>{

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "members", ignore = true)
	@Mapping(target = "manager", expression = "java(manager(source))")
	public abstract ProjectDTO to(ProjectRequest source);

	protected PersonDTO manager(ProjectRequest source) {
		return PersonDTO.builder().id(source.getManagerId()).build();
	}
	@Mapping(target = "managerId", source = "manager.id" )
	public abstract ProjectRequest from(ProjectDTO target) ;
}
