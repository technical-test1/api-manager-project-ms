package br.com.cunha.mps.controller.dto;

import java.time.LocalDate;

import br.com.cunha.mps.util.EmployeeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Tag(name = "PersonResponse" , description = "Retorna as novas informações do membro")
@Schema(name = "PersonResponse" , description = "Retorna as novas informações do membro")
public class PersonResponse {

	@Schema(name = "id", description = "Código único identificado do membro")
	private Long id;

	@Schema(name = "name", description = "Nome do membro da equipe")
	private String name;

	@Schema(name = "dateOfBirth", description = "Data de nascimento da equipe")
	private LocalDate dateOfBirth;

	@Schema(name = "cpf", description = "CPF do membro. Permitido gravar somente um membro por CPF")
	private String cpf;

	@Schema(name = "employee", description = "Identificador se o membro do Equipe é (F)uncionário ou (P)arceiro")
	private EmployeeEnum employee;
}