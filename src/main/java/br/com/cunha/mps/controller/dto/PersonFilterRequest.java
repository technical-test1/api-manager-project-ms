package br.com.cunha.mps.controller.dto;

import br.com.cunha.mps.util.EmployeeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Tag(name = "PersonFilterRequest" , description = "Parametros utilizados para efetuar uma pesquisa por filtros")
@Schema(name = "PersonFilterRequest" , description = "Parametros utilizados para efetuar uma pesquisa por filtros")
public class PersonFilterRequest {

	@Schema(name = "name" , description = "Pesquisa por parte do nome do membro")
	private String name;

	@Schema(name = "cpf" , description = "Pesquisa por CPF do membro")
	private String cpf;

	@Schema(name = "employee" , description = "Pesquisa por tipo de Colaborador")
	private EmployeeEnum employee;
}
