package br.com.cunha.mps.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cunha.mps.controller.dto.ProjectFilterRequest;
import br.com.cunha.mps.controller.dto.ProjectRequest;
import br.com.cunha.mps.controller.dto.ProjectResponse;
import br.com.cunha.mps.controller.mapper.ProjectFilterMapper;
import br.com.cunha.mps.controller.mapper.ProjectRequestMapper;
import br.com.cunha.mps.controller.mapper.ProjectResponseMapper;
import br.com.cunha.mps.controller.openapi.ProjectControllerOpenApi;
import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.domain.service.ProjectService;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import br.com.cunha.mps.util.StatusProjectEnum;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/project")
@RequiredArgsConstructor
@Slf4j(topic = "ProjectController")
public class ProjectController implements ProjectControllerOpenApi {

	private final ProjectRequestMapper projectRequestMapper;
	private final ProjectResponseMapper projectResponseMapper;
	private final ProjectFilterMapper projectFilterMapper;
	private final ProjectService projectService;

	@Override
	@PostMapping("/v1/projects" )
	public ResponseEntity<ResponseDTO<ProjectResponse>> post( @Valid @RequestBody ProjectRequest projectRequest ){

		log.info("Inciando o Request para gravar um novo projeto" );

		ProjectDTO projectDTO = projectRequestMapper.to(projectRequest);

		ProjectDTO responseDTO = projectService.save(projectDTO);

		ProjectResponse response = projectResponseMapper.to(responseDTO);

		return ResponseEntity
				.status( HttpStatus.CREATED )
				.body( new ResponseDTO<>( response ));
	}

	@Override
	@PutMapping("/v1/projects/{projectId}" )
	public ResponseEntity<ResponseDTO<ProjectResponse>> put(  @PathVariable Long projectId, @Valid @RequestBody ProjectRequest projectRequest ){

		log.info("Inciando o Request para altera\u00e7\u00e3o das informa\u00e7\u00f5es do projeto");

		ProjectDTO projectDTO = projectRequestMapper.to(projectRequest);
		projectDTO.setId(projectId);

		ProjectDTO responseDTO = projectService.save(projectDTO);

		ProjectResponse response = projectResponseMapper.to(responseDTO);

		return ResponseEntity
				.status( HttpStatus.CREATED )
				.body( new ResponseDTO<>( response ));
	}

	@Override
	@GetMapping("/v1/projects/{projectId}" )
	public ResponseEntity<ResponseDTO<ProjectResponse>> findById( @PathVariable Long projectId ){

		log.info("Inciando o Request para alteração das informações do projeto");

		ProjectDTO projectDTO = this.projectService.findById(projectId);
		ProjectResponse response = projectResponseMapper.to(projectDTO);

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( response ));
	}

	@Override
	@GetMapping("/v1/projects/find-by" )
	public ResponseEntity<ResponseDTO<PageDTO<ProjectResponse>>> findBy( ProjectFilterRequest projectFilterRequest, Integer page, Integer size ){

		log.info("Inciando o Request para alteração das informações do projeto");

		ProjectFilterDTO filterDTO = projectFilterMapper.to(projectFilterRequest);

		PageDTO<ProjectDTO> response = this.projectService.findBy(filterDTO, page, size);


		List<ProjectResponse> projectResponse = projectResponseMapper.to(response.getContent());

		PageDTO<ProjectResponse> pageResponse = new PageDTO<>(projectResponse);

		pageResponse.setFirst(response.isFirst());
		pageResponse.setLast(response.isLast());
		pageResponse.setNumber(response.getNumber());
		pageResponse.setNumberOfElements(response.getNumberOfElements());
		pageResponse.setSize(response.getSize());
		pageResponse.setTotalElements(response.getTotalElements());
		pageResponse.setTotalPages(response.getTotalPages());

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( pageResponse ));
	}

	@Override
	@PutMapping("/v1/projects/{projectId}/{status}" )
	public ResponseEntity<ResponseDTO<String>> putUpdateStatus( @PathVariable Long projectId, @PathVariable StatusProjectEnum status ){

		log.info("Inciando o Request para gravar um novo projeto");

		this.projectService.updateStatus(projectId, status);

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( "Status alterado com sucesso" ));
	}

	@Override
	@DeleteMapping("/v1/projects/{projectId}" )
	public ResponseEntity<ResponseDTO<String>> delete( @PathVariable Long projectId ){

		log.info("Inciando o Request para gravar um novo projeto");

		this.projectService.delete(projectId);

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( "Projeto removido com sucesso" ));
	}
}