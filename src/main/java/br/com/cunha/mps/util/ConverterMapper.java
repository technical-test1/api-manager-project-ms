package br.com.cunha.mps.util;

import java.util.List;

public interface ConverterMapper<S,T> {
	T to (S source);
	List<T> to (List<S> source);

	S from (T target );
	List<S> from (List<T> target );
}