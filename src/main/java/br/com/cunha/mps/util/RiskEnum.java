package br.com.cunha.mps.util;

import br.com.cunha.mps.infra.exception.ApiException;
import br.com.cunha.mps.infra.util.ExceptionCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RiskEnum {
	BAIXO("Baixo"),
	MEDIO("Médio"),
	ALTO("Alto"),
	;
	private String description;

	@Override
	public String toString() {
	    return name();
	}

	public static RiskEnum getRisk(String risk ) {
		for( RiskEnum r : RiskEnum.values() ) {
			if( r.name().equals(risk) ) {
				return r;
			}
		}
		throw new ApiException(ExceptionCodeEnum.RICK_CODE_NOT_CATALOGED);
	}

}
