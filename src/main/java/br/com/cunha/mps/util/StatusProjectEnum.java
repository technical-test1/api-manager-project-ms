package br.com.cunha.mps.util;

import br.com.cunha.mps.infra.exception.ApiException;
import br.com.cunha.mps.infra.util.ExceptionCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusProjectEnum {
	EM_ANALISE("Em analise","S"),
	ANALISE_REALIZADA("Analise Realizada","S"),
	ANALISE_APROVADA("Analise Aprovada","S"),
	INICIADO("Iniciado","N"),
	PLANEJADO("Planejado","S"),
	EM_ANDAMENTO("Em Andamento","N"),
	ENCERRADO("Encerrado","N"),
	CANCELADO("Cancelado","S"),
	;

	private String description;
	private String canDelete;

	public Boolean notCanDelete() {
		return this.canDelete.equals("N");
	}

	@Override
	public String toString() {
	    return name();
	}

	public static StatusProjectEnum getStatus(String status ) {
		for( StatusProjectEnum r : StatusProjectEnum.values() ) {
			if( r.name().equals(status) ) {
				return r;
			}
		}
		throw new ApiException(ExceptionCodeEnum.STATUS_CODE_NOT_CATALOGED);
	}
}
