package br.com.cunha.mps.util;

import org.apache.commons.lang3.BooleanUtils;

import lombok.Getter;

@Getter
public enum EmployeeEnum {

	F("Funcionario"),
	P("Parceiro"),
	;

	private String description;

	private EmployeeEnum(String description ) {
		this.description = description;
	}

	@Override
	public String toString() {
	    return name();
	}

	public Boolean notAnEmployee() {
		return BooleanUtils.isFalse(this.equals(F));
	}

}
