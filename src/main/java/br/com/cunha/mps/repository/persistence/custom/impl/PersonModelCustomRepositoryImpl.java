package br.com.cunha.mps.repository.persistence.custom.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.repository.model.PersonModel;
import br.com.cunha.mps.repository.persistence.custom.PersonModelCustomRepository;
import br.com.cunha.mps.util.EmployeeEnum;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PersonModelCustomRepositoryImpl implements PersonModelCustomRepository {

	private final EntityManager manager;

	@Override
	public Page<PersonModel> findBy(PersonFilterDTO personFilterDTO, Pageable sortedByName) {
		return new PageImpl<>(resultList(personFilterDTO, sortedByName), sortedByName, total(personFilterDTO) );
	}

	private List<PersonModel> resultList(PersonFilterDTO personFilterDTO, Pageable sortedByName) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<PersonModel> criteria = builder.createQuery(PersonModel.class);
		Root<PersonModel> root = criteria.from(PersonModel.class);
		Predicate[] predicates = createRestrictions(personFilterDTO, builder, root);
		criteria.where(predicates);
		criteria.orderBy( (builder.desc(root.get("name"))));
		TypedQuery<PersonModel> query = manager.createQuery(criteria);
		addRestrictionsOfPagnation(query, sortedByName );
		return query.getResultList();
	}

	private Long total( PersonFilterDTO personFilterDTO ) {
		CriteriaBuilder qb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		Root<PersonModel> root = cq.from(PersonModel.class);
		Predicate[] predicates = createRestrictions(personFilterDTO, qb, root);
		cq.select(qb.count(cq.from(PersonModel.class)));
		cq.where(predicates);
		return manager.createQuery(cq).getSingleResult();
	}

	static void addRestrictionsOfPagnation(TypedQuery<PersonModel> query, Pageable pageable) {
		int actualPage = pageable.getPageNumber();
		int totalForPage = pageable.getPageSize();
		int firstPage = actualPage * totalForPage;
		query.setFirstResult(firstPage);
		query.setMaxResults(totalForPage);
	}

	private Predicate[] createRestrictions(PersonFilterDTO filter, CriteriaBuilder builder, Root<PersonModel> root) {
		List<Predicate> predicates = new ArrayList<>();
		if (StringUtils.isNotBlank(filter.getName())) {
			predicates.add(builder.like( builder.upper(root.get( "name") ),
					StringUtils.upperCase( "%" +filter.getName().trim() + "%") ));
		}
		if (ObjectUtils.isNotEmpty(filter.getEmployee())) {
			predicates.add( builder.equal(  root.get("isEmployee"), EmployeeEnum.F.equals(filter.getEmployee()) ));

		}
		if (StringUtils.isNotBlank(filter.getCpf())) {
			predicates.add(builder.equal( builder.upper( root.get("cpf") ),
					StringUtils.upperCase( filter.getCpf() )));
		}
		return predicates.toArray(new Predicate[predicates.size()]);
	}
}