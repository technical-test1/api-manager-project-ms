package br.com.cunha.mps.repository.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberModelPK  implements Serializable {
	private Long personId;
	private Long projectId;
}