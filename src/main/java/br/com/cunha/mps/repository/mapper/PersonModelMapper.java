package br.com.cunha.mps.repository.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.repository.model.PersonModel;
import br.com.cunha.mps.util.ConverterMapper;
import br.com.cunha.mps.util.EmployeeEnum;

@Mapper(componentModel = "spring", uses = {})
public abstract class PersonModelMapper implements ConverterMapper<PersonDTO, PersonModel>{

	@Mapping(target = "isEmployee", expression = "java(  employee(source) )")
	@Mapping(target = "managerProjects", ignore = true)
	@Mapping(target = "projects", ignore = true)
	public abstract PersonModel to(PersonDTO source);

	@Mapping(target = "employee", expression = "java(  employee(target) )")
	@Mapping(target = "id", source = "id")
	public abstract PersonDTO from(PersonModel target);

	protected Boolean employee(PersonDTO source) {
		return EmployeeEnum.F.equals( source.getEmployee() );
	}

	protected EmployeeEnum employee(PersonModel target) {
		EmployeeEnum employeeEnum = EmployeeEnum.P;
		if ( Boolean.TRUE.equals(target.getIsEmployee()) ) {
			employeeEnum = EmployeeEnum.F;
		}
		return employeeEnum;
	}
}
