package br.com.cunha.mps.repository.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.domain.service.PersonRepositoryAdapter;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.repository.mapper.PersonModelMapper;
import br.com.cunha.mps.repository.model.PersonModel;
import br.com.cunha.mps.repository.persistence.custom.PersonModelRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Repository
@RequiredArgsConstructor
@Slf4j(topic = "PersonRepository")
public class PersonRepositoryImpl implements PersonRepositoryAdapter{

	private final PersonModelRepository personModelRepository;

	private final PersonModelMapper personModelMapper;

	@Override
	@Transactional
	public PersonDTO save(PersonDTO personDTO) {
		log.info("Iniciando o processo de persistir no banco de dados");
		PersonModel personModel = personModelMapper.to(personDTO);
		PersonModel response = personModelRepository.save(personModel);
		return personModelMapper.from(response);
	}

	@Override
	@Transactional
	public void delete(Long personId) {
		this.personModelRepository.deleteById(personId);
	}

	@Override
	public PersonDTO findById(Long personId) {
		Optional<PersonModel> optinal = this.personModelRepository.findById(personId);
		PersonModel response = optinal.orElse(null);
		return personModelMapper.from(response);
	}

	@Override
	public PageDTO<PersonDTO> findBy(PersonFilterDTO personFilterDTO, Integer page, Integer size ) {

		Pageable pageable = PageRequest.of(page, size, Sort.by("name"));

		Page<PersonModel> result = personModelRepository.findBy(personFilterDTO, pageable);

		List<PersonModel> content = result.getContent();

		List<PersonDTO> personDTOs = personModelMapper.from(content);

		PageDTO<PersonDTO> pageDTO = new PageDTO<>(personDTOs);
		pageDTO.setFirst(result.isFirst());
		pageDTO.setLast(result.isLast());
		pageDTO.setNumber(result.getNumber());
		pageDTO.setNumberOfElements(result.getNumberOfElements());
		pageDTO.setSize(result.getSize());
		pageDTO.setTotalElements(result.getTotalElements());
		pageDTO.setTotalPages(result.getTotalPages());
		return pageDTO;

	}

	@Override
	public PersonDTO findByCPF(String cpf) {
		Optional<PersonModel> optional = personModelRepository.findByCpf(cpf);
		PersonModel response = optional.orElse(null);
		return personModelMapper.from(response);
	}
}
