package br.com.cunha.mps.repository.mapper;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.repository.model.ProjectModel;
import br.com.cunha.mps.util.ConverterMapper;

@Mapper(componentModel = "spring", uses = {})
public abstract class ProjectModelMapper implements ConverterMapper<ProjectDTO, ProjectModel>{

	@Autowired
	protected PersonModelMapper personModelMapper;

	@Mapping(target = "members", ignore = true)
	@Mapping(target = "risk", source = "risk" )
	@Mapping(target = "status", source = "status" )
	@Mapping(target = "manager", expression = "java( personModelMapper.to(source.getManager()))" )
	public abstract ProjectModel to(ProjectDTO source);

	@Mapping(target = "members", expression =  "java( members(target))")
	@Mapping(target = "manager", expression =  "java( personModelMapper.from(target.getManager()))")
	public abstract ProjectDTO from(ProjectModel target);

	protected Set<PersonDTO> members(ProjectModel target){
		Set<PersonDTO> members = new HashSet<>();
		if( ObjectUtils.isNotEmpty( target.getMembers() ) ) {
			target.getMembers().forEach( m-> members.add( personModelMapper.from(m.getMember())));
		}
		return members;
	}

}