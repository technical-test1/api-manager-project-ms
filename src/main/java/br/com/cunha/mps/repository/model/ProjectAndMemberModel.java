package br.com.cunha.mps.repository.model;

import java.io.Serializable;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ConstraintMode;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table
@Entity( name = "MEMBROS")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@IdClass(MemberModelPK.class)
public class ProjectAndMemberModel  implements Serializable {

	@Id
	@Column(name = "IDPROJETO")
	@EqualsAndHashCode.Include
	private Long projectId;

	@Id
	@Column(name = "IDPESSOA")
	@EqualsAndHashCode.Include
	private Long personId;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY, orphanRemoval = false)
	@JoinColumn(name = "IDPESSOA", foreignKey = @ForeignKey( value =ConstraintMode.CONSTRAINT , name = "fk_membros_pessoa"), updatable = false, insertable = false )
	@ToString.Exclude
	public PersonModel member;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY, orphanRemoval = false)
	@JoinColumn(name = "IDPROJETO", foreignKey = @ForeignKey( value =ConstraintMode.CONSTRAINT , name = "fk_membros_projeto"), updatable = false, insertable = false )
	@ToString.Exclude
	public ProjectModel project;

}
