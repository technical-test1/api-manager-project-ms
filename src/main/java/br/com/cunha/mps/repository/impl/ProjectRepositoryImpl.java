package br.com.cunha.mps.repository.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.domain.service.ProjectRepositoryAdapter;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.exception.ApiException;
import br.com.cunha.mps.infra.util.ExceptionCodeEnum;
import br.com.cunha.mps.repository.mapper.ProjectModelMapper;
import br.com.cunha.mps.repository.model.ProjectModel;
import br.com.cunha.mps.repository.persistence.custom.ProjectModelRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Repository
@RequiredArgsConstructor
@Slf4j(topic = "ProjectRepository")
public class ProjectRepositoryImpl implements ProjectRepositoryAdapter {

	private final ProjectModelRepository projectModelRepository;
	private final ProjectModelMapper projectModelMapper;

	@Override
	@Transactional
	public ProjectDTO save(ProjectDTO projectDTO) {

		log.info("Gravando as informações do projeto");
		ProjectModel projectModel = projectModelMapper.to(projectDTO);
		ProjectModel result = projectModelRepository.save(projectModel);

		return projectModelMapper.from(result);
	}

	@Override
	@Transactional
	public void deleteById(Long projectId) {
		projectModelRepository.deleteById(projectId);
	}

	@Override
	public ProjectDTO findById(Long projectId) {
		ProjectModel projectModel = projectModelRepository
				.findById(projectId)
				.orElseThrow( () -> new ApiException(ExceptionCodeEnum.PROJECT_NOT_FOUND) );

		return projectModelMapper.from(projectModel);
	}



	@Override
	public PageDTO<ProjectDTO> findBy(ProjectFilterDTO projectFilterDTO, Integer page, Integer size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("name"));
		Page<ProjectModel> result = projectModelRepository.findBy(projectFilterDTO, pageable);

		List<ProjectDTO> projectDTOs = projectModelMapper.from(result.getContent());

		PageDTO<ProjectDTO> pageDTO = new PageDTO<>(projectDTOs);
		pageDTO.setFirst(result.isFirst());
		pageDTO.setLast(result.isLast());
		pageDTO.setNumber(result.getNumber());
		pageDTO.setNumberOfElements(result.getNumberOfElements());
		pageDTO.setSize(result.getSize());
		pageDTO.setTotalElements(result.getTotalElements());
		pageDTO.setTotalPages(result.getTotalPages());
		return pageDTO;
	}

	@Override
	@Transactional
	public void updateStatus(ProjectDTO projectDTO) {

		ProjectModel projectModel = projectModelRepository
				.findById(projectDTO.getId())
				.orElseThrow( () -> new ApiException(ExceptionCodeEnum.PROJECT_NOT_FOUND) );

		projectModel.setStatus( projectDTO.getStatus());
		projectModel.setEndDate( projectDTO.getEndDate());

		projectModelRepository.save(projectModel);

	}
}
