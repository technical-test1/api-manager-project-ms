package br.com.cunha.mps.repository.persistence.custom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.repository.model.ProjectModel;

public interface ProjectModelCustomRepository {

	Page<ProjectModel> findBy( ProjectFilterDTO projectFilterDTO, Pageable sortedByName );
}
