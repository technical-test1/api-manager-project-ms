package br.com.cunha.mps.repository.persistence.custom;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cunha.mps.repository.model.PersonModel;


public interface PersonModelRepository extends JpaRepository<PersonModel, Long>, PersonModelCustomRepository {

	Optional<PersonModel> findByCpf(String cpf);
}
