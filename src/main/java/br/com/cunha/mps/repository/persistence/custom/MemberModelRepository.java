package br.com.cunha.mps.repository.persistence.custom;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cunha.mps.repository.model.MemberModelPK;
import br.com.cunha.mps.repository.model.ProjectAndMemberModel;

public interface MemberModelRepository extends JpaRepository<ProjectAndMemberModel, MemberModelPK> {

	void deleteByProjectIdAndPersonIdIn(Long projectId, Set<Long> personId);

	Boolean existsByPersonId( Long memberId );
}