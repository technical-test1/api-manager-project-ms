package br.com.cunha.mps.repository.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

//@Table(uniqueConstraints = @UniqueConstraint( columnNames = "CPF", name = Constraint.UK_PERSON_CPF))
@Table
@Entity( name = "PESSOA")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class PersonModel  implements Serializable {
	@Id
	@SequenceGenerator(name = "TB_PERSON_GENERATOR", sequenceName = "SEQ_PERSON", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "TB_PERSON_GENERATOR")
	@Column(name = "ID")
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name="NOME")
	private String name;

	@Column(name="DATANASCIMENTO")
	private LocalDate dateOfBirth;

	@Column(name = "CPF" )
	private String cpf;

	@Column(name = "FUNCIONARIO")
	private Boolean isEmployee;

	@OneToMany(mappedBy = "manager", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH )
	private Set<ProjectModel> managerProjects;

	@OneToMany
	@JoinColumn(name = "IDPESSOA" , updatable = false, insertable = false )
	@ToString.Exclude
	private Set<ProjectAndMemberModel> projects;
}
