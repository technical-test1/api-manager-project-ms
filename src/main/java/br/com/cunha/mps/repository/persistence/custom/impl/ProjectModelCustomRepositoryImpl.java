package br.com.cunha.mps.repository.persistence.custom.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.repository.model.PersonModel;
import br.com.cunha.mps.repository.model.ProjectAndMemberModel;
import br.com.cunha.mps.repository.model.ProjectModel;
import br.com.cunha.mps.repository.persistence.custom.ProjectModelCustomRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProjectModelCustomRepositoryImpl implements ProjectModelCustomRepository {

	private final EntityManager manager;

	@Override
	public Page<ProjectModel> findBy(ProjectFilterDTO projectFilterDTO, Pageable sortedByName) {
		return new PageImpl<>(resultList(projectFilterDTO, sortedByName), sortedByName, total(projectFilterDTO) );
	}

	private List<ProjectModel> resultList(ProjectFilterDTO projectFilterDTO, Pageable sortedByName) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ProjectModel> criteria = builder.createQuery(ProjectModel.class);
		Root<ProjectModel> root = criteria.from(ProjectModel.class);
		Predicate[] predicates = createRestrictions(projectFilterDTO, builder, root);
		criteria.where(predicates);
		criteria.orderBy( (builder.desc(root.get("name"))));
		TypedQuery<ProjectModel> query = manager.createQuery(criteria);
		addRestrictionsOfPagnation(query, sortedByName );
		return query.getResultList();
	}

	private Long total( ProjectFilterDTO projectFilterDTO ) {
		CriteriaBuilder qb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		Root<ProjectModel> root = cq.from(ProjectModel.class);
		Predicate[] predicates = createRestrictions(projectFilterDTO, qb, root);
		cq.select(qb.count(cq.from(ProjectModel.class)));
		cq.where(predicates);
		return manager.createQuery(cq).getSingleResult();
	}

	static void addRestrictionsOfPagnation(TypedQuery<ProjectModel> query, Pageable pageable) {
		int actualPage = pageable.getPageNumber();
		int totalForPage = pageable.getPageSize();
		int firstPage = actualPage * totalForPage;
		query.setFirstResult(firstPage);
		query.setMaxResults(totalForPage);
	}

	private Predicate[] createRestrictions(ProjectFilterDTO filter, CriteriaBuilder builder, Root<ProjectModel> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (StringUtils.isNotBlank(filter.getName()) ) {
			predicates.add(builder.like( builder.upper(root.get( "name") ),
					StringUtils.upperCase( "%" +filter.getName().trim() + "%") ));
		}

		if (filter.getManegerId() != null ) {
			Join<ProjectModel, PersonModel> personJoin = root.join("manager", JoinType.LEFT);
			predicates.add(builder.equal(personJoin.get("id"), filter.getManegerId()));
		}

		if (filter.getMemberId() != null ) {
			Join<ProjectModel, ProjectAndMemberModel> projectAndMemberJoin = root.join("members", JoinType.LEFT );
			predicates.add(builder.equal(projectAndMemberJoin.get("personId"), filter.getMemberId()));
		}

		if( filter.getStartDate() != null ) {
			predicates.add(builder.greaterThan(root.get("startDate"), filter.getStartDate()));
		}

		if( filter.getEndDate() != null ) {
			predicates.add(builder.lessThan(root.get("endDate"), filter.getEndDate()));
		}

		if( filter.getStartExpectedDate() != null ) {
			predicates.add(builder.greaterThan(root.get("expectedEndDate"), filter.getStartExpectedDate()));
		}

		if( filter.getEndExpectedDate() != null ) {
			predicates.add(builder.lessThan(root.get("expectedEndDate"), filter.getEndExpectedDate()));
		}

		if( filter.getRisk() != null ) {
			predicates.add(builder.equal(root.get("risk"), filter.getRisk().name()));
		}

		if( filter.getStatus() != null ) {
			predicates.add(builder.equal(root.get("status"), filter.getStatus().name()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}
}