package br.com.cunha.mps.repository.persistence.custom;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cunha.mps.repository.model.ProjectModel;

public interface ProjectModelRepository extends JpaRepository<ProjectModel, Long>, ProjectModelCustomRepository {

}