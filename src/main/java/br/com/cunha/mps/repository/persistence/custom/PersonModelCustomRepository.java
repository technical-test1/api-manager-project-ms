package br.com.cunha.mps.repository.persistence.custom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.repository.model.PersonModel;

public interface PersonModelCustomRepository {

	Page<PersonModel> findBy( PersonFilterDTO personFilterDTO, Pageable sortedByName );
}
