package br.com.cunha.mps.repository.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Repository;

import br.com.cunha.mps.domain.service.ManagementRepositoryAdapter;
import br.com.cunha.mps.repository.model.ProjectAndMemberModel;
import br.com.cunha.mps.repository.persistence.custom.MemberModelRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ManagementRepositoryImpl implements ManagementRepositoryAdapter{

	private final MemberModelRepository memberModelRepository;
	@Override
	@Transactional
	public void associateMembers(Long projectId, Set<Long> membersId) {

		Set<ProjectAndMemberModel> memberModels = new HashSet<>();

		membersId.forEach( m-> memberModels
				.add( ProjectAndMemberModel
						.builder()
						.projectId(projectId)
						.personId(m)
							.build() ) );

		memberModelRepository.saveAll(memberModels);
	}

	@Override
	@Transactional
	public void disassociateMembers(Long projectId, Set<Long> membersId) {
		memberModelRepository.deleteByProjectIdAndPersonIdIn(projectId,membersId);
	}

	@Override
	public Boolean existsMemberAssociate(Long memberId) {
		return memberModelRepository.existsByPersonId(memberId);
	}
}