package br.com.cunha.mps.repository.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import br.com.cunha.mps.util.RiskEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ConstraintMode;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table
@Entity( name = "PROJETO")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class ProjectModel implements Serializable {

	@Id
	@SequenceGenerator(name = "TB_PROJECT_GENERATOR", sequenceName = "SEQ_PROJECT", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_PROJECT_GENERATOR")
	@Column(name = "ID")
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name = "NOME")
	private String name;

	@Column(name = "DATA_INICIO")
	private LocalDate startDate;

	@Column(name = "DATA_PREVISAO_FIM")

	private LocalDate expectedEndDate;

	@Column(name = "DATA_FIM")
	private LocalDate endDate;

	@Column(name = "DESCRICAO")
	private String description;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "RISCO")
	private String risk;

	@Column(name = "ORCAMENTO")
	private BigDecimal budget;


	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER )
	@JoinColumn(name = "IDGERENTE", foreignKey = @ForeignKey( value =ConstraintMode.CONSTRAINT , name = "fk_gerente"))
	@ToString.Exclude
	private PersonModel manager;

	@OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPROJETO" )
	@ToString.Exclude
	private Set<ProjectAndMemberModel> members;

	public void setRisk(RiskEnum riskEnum) {
		this.risk = riskEnum.name();
	}

	public RiskEnum getRisk() {
		return RiskEnum.getRisk(risk);
	}

	public void setStatus(StatusProjectEnum status) {
		this.status = status.name();
	}

	public StatusProjectEnum getStatus() {
		return StatusProjectEnum.getStatus(status);
	}

}