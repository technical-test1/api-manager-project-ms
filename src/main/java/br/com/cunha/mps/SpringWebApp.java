package br.com.cunha.mps;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import br.com.cunha.mps.infra.dto.ApiProperties;
import lombok.extern.slf4j.Slf4j;

@EnableConfigurationProperties( value = {ApiProperties.class} )
@SpringBootApplication()
@Slf4j
@EnableScheduling
public class SpringWebApp {

	private static final String LINHA_SEPARADORA = StringUtils.repeat("-", 127);

	public static void main(String[] args) throws UnknownHostException {

		final SpringApplication app = new SpringApplication(SpringWebApp.class);
		final Environment env = app.run(args).getEnvironment();
		String protocol = "http";

		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}

		log.info(LINHA_SEPARADORA);
		log.info( String.format("SpringWebApp:   %s", env.getProperty("spring.application.name") ) );
		log.info( String.format("Versão:         %s", env.getProperty("app.version") ) );
		log.info( String.format("Local:          %s://localhost:%s" , protocol , env.getProperty("server.port") ) );
		log.info( String.format("External:       %s://%s:%s" , protocol , InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port") ) );
		log.info( String.format("Database:       %s / %s" , env.getProperty("spring.datasource.url") , env.getProperty("spring.datasource.username") ) );
		log.info( String.format("Profiles:       %s", Arrays.asList(env.getActiveProfiles() ) ) );
		log.info(LINHA_SEPARADORA);

	}

}
