package br.com.cunha.mps.domain.service.impl;

import java.util.Set;

import org.springframework.stereotype.Service;

import br.com.cunha.mps.domain.service.ManagementRepositoryAdapter;
import br.com.cunha.mps.domain.service.ManagementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j(topic = "ManagementService")
@RequiredArgsConstructor
public class ManagementServiceImpl implements ManagementService {


	private final ManagementRepositoryAdapter managementRepository;
	@Override
	public void associateMembers(Long projectId, Set<Long> membersId) {
		log.info("Iniciando o processo de associar os membros no projeto");
		this.managementRepository.associateMembers(projectId, membersId);
	}

	@Override
	public void disassociateMembers(Long projectId, Set<Long> membersId) {
		log.info("Iniciando o processo de desassociar os membros no projeto");
		this.managementRepository.disassociateMembers(projectId, membersId);
	}

	@Override
	public Boolean existsMemberAssociate(Long memberId) {
		return managementRepository.existsMemberAssociate(memberId);
	}

}
