package br.com.cunha.mps.domain.service;

import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.util.StatusProjectEnum;

public interface ProjectService {

	ProjectDTO save( ProjectDTO projectDTO );
	void delete( Long projectDTO );
	void updateStatus( Long projectDTO , StatusProjectEnum statusProjectEnum );
	ProjectDTO findById( Long projectDTO );
	PageDTO<ProjectDTO> findBy( ProjectFilterDTO projectFilterDTO, Integer page, Integer size );
}
