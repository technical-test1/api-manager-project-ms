package br.com.cunha.mps.domain.service.impl;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.domain.service.ManagementService;
import br.com.cunha.mps.domain.service.PersonRepositoryAdapter;
import br.com.cunha.mps.domain.service.PersonService;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.exception.ApiException;
import br.com.cunha.mps.infra.util.ExceptionCodeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j(topic = "PersonService")
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

	private final PersonRepositoryAdapter personRepository;
	private final ManagementService managementService;


	@Override
	public PersonDTO save(PersonDTO personDTO) {
		log.info("Inciando a gravação das informações de um novo membro");
		validatePerson(personDTO);
		return personRepository.save(personDTO);
	}

	private void validatePerson(PersonDTO personDTO) {
		if(ObjectUtils.isEmpty( personDTO.getCpf() ) ) {
			return;
		}
		PersonDTO person = personRepository.findByCPF(personDTO.getCpf());

		if( person!= null && ObjectUtils.notEqual(personDTO, person)) {
			throw new ApiException(ExceptionCodeEnum.MEMBER_CPF_FOUND);
		}
	}

	@Override
	public void delete(Long personId) {

		PersonDTO person = personRepository.findById(personId);
		if( person == null ) {
			throw new ApiException(ExceptionCodeEnum.MEMBER_NOT_FOUND_WITH_CODE_INFORMED);
		}

		if ( Boolean.TRUE.equals(managementService.existsMemberAssociate(personId)) ) {
			throw new ApiException(ExceptionCodeEnum.ASSOCIATED_MEMBER_IN_ONE_OR_MORE_PROJECTS);
		}

		this.personRepository.delete(personId);
	}

	@Override
	public PersonDTO findById(Long personId) {
		return personRepository.findById(personId);
	}

	@Override
	public PageDTO<PersonDTO> findBy(PersonFilterDTO personFilterDTO, Integer page, Integer size) {
		return personRepository.findBy(personFilterDTO, page, size);
	}
}
