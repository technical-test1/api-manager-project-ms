package br.com.cunha.mps.domain.service.impl;

import java.time.LocalDate;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.domain.service.PersonRepositoryAdapter;
import br.com.cunha.mps.domain.service.ProjectRepositoryAdapter;
import br.com.cunha.mps.domain.service.ProjectService;
import br.com.cunha.mps.infra.dto.PageDTO;
import br.com.cunha.mps.infra.exception.ApiException;
import br.com.cunha.mps.infra.util.ExceptionCodeEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j(topic = "ProjectServiceImpl")
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

	private final ProjectRepositoryAdapter projectRepositoryAdapter;
	private final PersonRepositoryAdapter personRepositoryAdapter;

	@Override
	public ProjectDTO save(ProjectDTO projectDTO) {
		log.info("Iniciando o processo de gravação do projeto");
		log.info("public ProjectDTO save(ProjectDTO personDTO)");

		PersonDTO manager = personRepositoryAdapter.findById( projectDTO.getManager().getId() );

		if( org.springframework.util.ObjectUtils.isEmpty( manager ) ) {
			throw new ApiException(ExceptionCodeEnum.MANAGER_INFORMED_NOT_FOUND);
		}

		if( BooleanUtils.isTrue( manager.getEmployee().notAnEmployee())) {
			throw new ApiException(ExceptionCodeEnum.MANAGER_INFORMED_NOT_EMPLOYEE);
		}

		return projectRepositoryAdapter.save(projectDTO);
	}

	@Override
	public void delete(Long projectId) {
		log.info("Iniciando o processo de deletar o projeto");

		ProjectDTO projectDTO = projectRepositoryAdapter.findById(projectId);

		if( CollectionUtils.isNotEmpty( projectDTO.getMembers() )) {
			throw new ApiException(ExceptionCodeEnum.PROJECT_NOT_CAN_REMOVED_MEMBERS_ASSOCIATE ); // . Status:
		}

		if( Boolean.TRUE.equals(projectDTO.getStatus().notCanDelete())) {
			throw new ApiException(ExceptionCodeEnum.PROJECT_NOT_CAN_REMOVED ); // . Status:
		}

		this.projectRepositoryAdapter.deleteById(projectId);
	}

	@Override
	public void updateStatus(Long projectId, StatusProjectEnum statusProjectEnum) {
		log.info("Iniciando o processo de alterar o status do projeto");
		ProjectDTO project = projectRepositoryAdapter.findById(projectId);

		if( StatusProjectEnum.ENCERRADO.equals(statusProjectEnum)) {
			project.setEndDate( LocalDate.now());
		}

		project.setStatus(statusProjectEnum);

		this.projectRepositoryAdapter.updateStatus(project);
	}

	@Override
	public ProjectDTO findById(Long projectId) {
		log.info("Iniciando o processo de consultar projeto por ID");
		return this.projectRepositoryAdapter.findById(projectId);
	}

	@Override
	public PageDTO<ProjectDTO> findBy(ProjectFilterDTO projectFilterDTO, Integer page, Integer size) {
		return this.projectRepositoryAdapter.findBy(projectFilterDTO, page, size);
	}
}
