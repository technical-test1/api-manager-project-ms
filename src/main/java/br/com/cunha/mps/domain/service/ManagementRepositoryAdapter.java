package br.com.cunha.mps.domain.service;

import java.util.Set;

public interface ManagementRepositoryAdapter {

	public void associateMembers(Long projectId, Set<Long> membersId );
	public void disassociateMembers(Long projectId, Set<Long> membersId );
	public Boolean existsMemberAssociate( Long memberId );
}
