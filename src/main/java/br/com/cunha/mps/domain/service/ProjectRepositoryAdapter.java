package br.com.cunha.mps.domain.service;

import br.com.cunha.mps.domain.dto.ProjectDTO;
import br.com.cunha.mps.domain.dto.ProjectFilterDTO;
import br.com.cunha.mps.infra.dto.PageDTO;

public interface ProjectRepositoryAdapter {
	ProjectDTO save( ProjectDTO personDTO );
	void deleteById( Long personId );
	ProjectDTO findById( Long personId );
	PageDTO<ProjectDTO> findBy( ProjectFilterDTO projectFilterDTO, Integer page, Integer size );
	void updateStatus(ProjectDTO project);
}
