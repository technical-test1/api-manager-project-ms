package br.com.cunha.mps.domain.dto;

import java.time.LocalDate;

import br.com.cunha.mps.util.RiskEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectFilterDTO {
	private String name;
	private Long manegerId;
	private Long memberId;
	private LocalDate startDate;
	private LocalDate endDate;
	private LocalDate startExpectedDate;
	private LocalDate endExpectedDate;
	private RiskEnum risk;
	private StatusProjectEnum status;

}
