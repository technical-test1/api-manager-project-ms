package br.com.cunha.mps.domain.service;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.infra.dto.PageDTO;

public interface PersonService {

	PersonDTO save( PersonDTO personDTO );

	void delete( Long personId );

	PersonDTO findById( Long personId );

	PageDTO<PersonDTO> findBy( PersonFilterDTO personFilterDTO, Integer page, Integer size );

}
