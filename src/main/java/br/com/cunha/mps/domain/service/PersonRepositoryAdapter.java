package br.com.cunha.mps.domain.service;

import br.com.cunha.mps.domain.dto.PersonDTO;
import br.com.cunha.mps.domain.dto.PersonFilterDTO;
import br.com.cunha.mps.infra.dto.PageDTO;

public interface PersonRepositoryAdapter {

	PersonDTO save( PersonDTO personDTO );
	void delete( Long personId );
	PersonDTO findByCPF( String cpf );
	PersonDTO findById( Long personId );
	PageDTO<PersonDTO> findBy( PersonFilterDTO personFilterDTO, Integer page, Integer size );
}