package br.com.cunha.mps.domain.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import br.com.cunha.mps.util.RiskEnum;
import br.com.cunha.mps.util.StatusProjectEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProjectDTO {

	@EqualsAndHashCode.Include
	private Long id;
	private String name;
	private LocalDate startDate;
	private LocalDate expectedEndDate;
	private LocalDate endDate;
	private String description;
	private StatusProjectEnum status;
	private BigDecimal budget;
	private RiskEnum risk;
	private PersonDTO manager;
	private Set<PersonDTO> members;

}
