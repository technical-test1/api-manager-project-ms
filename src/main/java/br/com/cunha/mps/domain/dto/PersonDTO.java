package br.com.cunha.mps.domain.dto;

import java.io.Serializable;
import java.time.LocalDate;

import br.com.cunha.mps.util.EmployeeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PersonDTO implements Serializable{

	@EqualsAndHashCode.Include
	private Long id;
	private String name;
	private LocalDate dateOfBirth;
	private String cpf;
	private EmployeeEnum employee;
}
