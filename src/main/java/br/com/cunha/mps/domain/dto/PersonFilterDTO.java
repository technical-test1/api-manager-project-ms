package br.com.cunha.mps.domain.dto;

import br.com.cunha.mps.util.EmployeeEnum;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonFilterDTO {
	private String name;
	private String cpf;
	private EmployeeEnum employee;
}
