package br.com.cunha.mps.infra.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "ApiErrorDTO", description = "Contém o código e descrição do erro")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ApiErrorDTO {

	@Schema(name = "code", description = "Código do erro interno")
	@EqualsAndHashCode.Include
	private String code;

	@Schema(name = "description", description = "Descrição do erro")
	private String description;
}
