package br.com.cunha.mps.infra;


import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@Configuration
@OpenAPIDefinition(info = @Info(
    title = "Teste Tecnico - Gerenciar Membros e Projetos",
    version = "${app.version}",
    description = "Gestão de Projetos",
    contact = @Contact(email = "leandromcunha@gmail.com" , name = "Leandro Marques da Cunha"),
    license = @License(name = "H2 Console", url =  "h2-console"),
    summary = "Aqui a ideia é expressar Designer de API, Arquitetura Limpa, JPA, SpringBoot, StructMap e Lombok"

))
public class SwaggerConfig {

}
