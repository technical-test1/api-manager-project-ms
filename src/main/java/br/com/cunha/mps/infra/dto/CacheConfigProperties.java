package br.com.cunha.mps.infra.dto;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "cache")
public class CacheConfigProperties {

    private long timeoutSeconds = 60;
    private int redisPort;
    private String redisHost;

    private Map<String, Long> cacheExpirations = new HashMap<>();

    CacheConfigProperties( @Value("${spring.redis.port}") int redisPort, @Value("${spring.redis.host}") String redisHost) {
      this.redisPort = redisPort;
      this.redisHost = redisHost;
    }
}
