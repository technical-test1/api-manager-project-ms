package br.com.cunha.mps.infra.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReflectionDTO {
	private String name;
	private Integer id;
	private LocalDateTime date;
	private Boolean isActive;
	private BigDecimal salary;
	private Long lastDays;
	private Double percent;
}
