package br.com.cunha.mps.infra.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Tag(name = "ResponseDTO")
@Schema( title = "ResponseDTO", description = "Resposta da requisição" )
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO<T>  {

	@Schema( title = "data", description = "Conteúda da resposta da requisição" )
	@JsonInclude(Include.NON_NULL)
	public T data;

}
