package br.com.cunha.mps.infra.util;

import org.springframework.http.HttpStatus;

public enum ExceptionCodeEnum {

    INTERNAL_SERVER_ERROR( HttpStatus.INTERNAL_SERVER_ERROR),
    MEMBER_CPF_FOUND(HttpStatus.NOT_FOUND),
    MEMBER_NOT_FOUND_WITH_CODE_INFORMED(HttpStatus.NOT_FOUND),
    PROJECT_NOT_FOUND(HttpStatus.NOT_FOUND),
    PROJECT_NOT_CAN_REMOVED(HttpStatus.NOT_FOUND),
    MANAGER_INFORMED_NOT_EMPLOYEE(HttpStatus.NOT_FOUND),
    RICK_CODE_NOT_CATALOGED(HttpStatus.NOT_FOUND),
    STATUS_CODE_NOT_CATALOGED(HttpStatus.NOT_FOUND),
    MANAGER_INFORMED_NOT_FOUND(HttpStatus.NOT_FOUND),
    PROJECT_NOT_CAN_REMOVED_MEMBERS_ASSOCIATE(HttpStatus.NOT_FOUND),
    ASSOCIATED_MEMBER_IN_ONE_OR_MORE_PROJECTS(HttpStatus.NOT_FOUND),
    ;

    private HttpStatus httpStatus;

    ExceptionCodeEnum( final HttpStatus httpStatus ) {
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return this.name();
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
}
