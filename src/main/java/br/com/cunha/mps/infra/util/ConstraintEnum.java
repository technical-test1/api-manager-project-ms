package br.com.cunha.mps.infra.util;

public enum ConstraintEnum {

	FK_MEMBROS_PESSOA(Constraint.FK_MEMBROS_PESSOA),
	UNDEFINED("UNDEFINED"),
	;

	private String name;

	private ConstraintEnum(String name ) {
		this.name = name;
	}

	public static ConstraintEnum getConstraint( String messageErro ) {
		for ( ConstraintEnum v : ConstraintEnum.values() ) {
			if( messageErro.contains( v.name )) {
				return v;
			}
		}
		return UNDEFINED;
	}

	public String getName() {
		return name;
	}
}
