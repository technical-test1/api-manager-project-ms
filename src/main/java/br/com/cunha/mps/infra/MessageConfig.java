package br.com.cunha.mps.infra;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import br.com.cunha.mps.infra.util.ExceptionCodeEnum;

@Component
public class MessageConfig {

	@Autowired
	private MessageSource messageSource;

	public String message(ExceptionCodeEnum mensagem) {
		return message(mensagem.getCode());
	}

	public String message(ExceptionCodeEnum mensagem , Object... args ) {
		return message(mensagem.getCode(), args);
	}

	public String message(String mensagem) {
		return messageSource.getMessage(mensagem, null, mensagem, Locale.getDefault());
	}

	public String message(String mensagem, Object... args ) {
		return messageSource.getMessage(mensagem, args, mensagem, Locale.getDefault());
	}
}
