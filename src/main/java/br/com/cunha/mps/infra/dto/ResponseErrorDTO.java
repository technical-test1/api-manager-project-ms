package br.com.cunha.mps.infra.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Tag(name = "ResponseErrorDTO")
@Schema( name = "ResponseErrorDTO", description = "Resposta de erro da requisição" )
public class ResponseErrorDTO  {


	@Schema( name = "error", description = "Mensagem de erro da requisição" )
	private APIError error;

	@Override
	public String toString() {
		return "ResponseErrorDTO [error=" + error + "]";
	}

}
