package br.com.cunha.mps.infra.active;

import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.ServletWebRequest;

import br.com.cunha.mps.infra.MessageConfig;
import br.com.cunha.mps.infra.dto.APIError;
import br.com.cunha.mps.infra.dto.ApiErrorDTO;
import br.com.cunha.mps.infra.dto.ApiProperties;
import br.com.cunha.mps.infra.dto.ResponseErrorDTO;
import br.com.cunha.mps.infra.util.ExceptionCodeEnum;


public class APIHandler {

	protected static final String MESSAGE_DEFAULT_EXCEPTION = "Uma Exception foi lancada:";

	protected static final Logger LOG = LoggerFactory.getLogger(APIHandler.class);

    protected ApiProperties properties;
    protected MessageConfig messageConfig;

    protected ApiErrorDTO apiErrorDTO( ExceptionCodeEnum exceptionCodeEnum) {

    	return ApiErrorDTO
    				.builder()
    				.code( exceptionCodeEnum.getCode() )
    				.description(this.messageConfig.message(exceptionCodeEnum))
    				.build();
    }

	protected ResponseEntity<ResponseErrorDTO> responseEntity(final ServletWebRequest request, String version,
			ApiErrorDTO message, HttpStatus httpStatus) {
		return responseEntity(request, version, Arrays.asList(message),httpStatus);
	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(final ServletWebRequest request, String version,
			Collection<ApiErrorDTO> messages, HttpStatus httpStatus) {

		APIError message =new APIError(version, httpStatus.value(),
				messages,  request.getRequest().getRequestURI(),
				request.getRequest().getMethod());
		return responseEntity(message);

	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(APIError messages) {
		LOG.info( "{}" , messages );
		ResponseErrorDTO body = new ResponseErrorDTO(messages);
		LOG.info( "{}" , body );
		return ResponseEntity.status(HttpStatus.valueOf(messages.getStatus())).body(body);
	}
}
