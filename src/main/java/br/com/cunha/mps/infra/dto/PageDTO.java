package br.com.cunha.mps.infra.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageDTO<T> {

	private int number;

	private int size;

	private int numberOfElements;

	private List<T> content;

	private boolean isFirst;

	private boolean isLast;

	private int totalPages;

	private long totalElements;

	public PageDTO(List<T> content) {
		this.content = content;
	}
}