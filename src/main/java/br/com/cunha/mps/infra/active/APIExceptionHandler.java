package br.com.cunha.mps.infra.active;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;

import br.com.cunha.mps.infra.MessageConfig;
import br.com.cunha.mps.infra.dto.ApiErrorDTO;
import br.com.cunha.mps.infra.dto.ApiProperties;
import br.com.cunha.mps.infra.dto.ResponseErrorDTO;

@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class APIExceptionHandler extends APIHandler {

	public APIExceptionHandler(MessageConfig messageConfig, ApiProperties properties){
		this.messageConfig = messageConfig;
		this.properties = properties;
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseErrorDTO> handleException(final Exception ex, final ServletWebRequest request) {
		LOG.error(MESSAGE_DEFAULT_EXCEPTION, ex );
		ApiErrorDTO error = ApiErrorDTO
			.builder()
			.code("999")
			.description(ex.getMessage())
			.build();
		return responseEntity(request, properties.getVersion(), error ,HttpStatus.BAD_REQUEST);
	}
}
