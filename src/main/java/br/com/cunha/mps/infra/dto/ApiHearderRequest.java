package br.com.cunha.mps.infra.dto;

import java.util.Collection;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties( "header-request" )
@Data
public class ApiHearderRequest {

	private Map<String,Map<String, Collection<String>>> headers;
	
	public Map<String, Collection<String>> config( String key ){
		return headers.get(key);
	}
}
