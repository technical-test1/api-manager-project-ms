package br.com.cunha.mps.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.cunha.mps.SpringWebApp;
import br.com.cunha.mps.configuration.TestBeanConfiguration;
import br.com.cunha.mps.controller.dto.ManagerRequest;
import br.com.cunha.mps.infra.dto.ResponseDTO;
import br.com.cunha.mps.repository.persistence.custom.MemberModelRepository;

@AutoConfigureMockMvc
@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = "classpath:application-test.yml")
@SpringBootTest(classes = { SpringWebApp.class, TestBeanConfiguration.class})
class ManagementControllerTest {

	private final static String URL = "/api/manager";

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private MemberModelRepository memberModelRepository;

	@Test
	void test_associate_member_with_sucess() throws JsonProcessingException, Exception {

		var request = ManagerRequest.builder().membersId(Collections.singleton(1l)).build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post(URL.concat("/v1/manager/associate-members/1"))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers
						.status()
						.isOk())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseDTO<String> response = objectMapper.readValue(contentAsString,
				new TypeReference<ResponseDTO<String>>() {
		});

		assertNotNull(  response.getData()  );

	}

	@Test
	void test_desassociate_member_with_sucess() throws JsonProcessingException, Exception {

		var request = ManagerRequest.builder().membersId(Collections.singleton(1l)).build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post(URL.concat("/v1/manager/disassociate-members/1"))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers
						.status()
						.isOk())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseDTO<String> response = objectMapper.readValue(contentAsString,
				new TypeReference<ResponseDTO<String>>() {
		});

		assertNotNull(  response.getData()  );

	}

}
