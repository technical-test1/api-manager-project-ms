package br.com.cunha.mps.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.core.util.ObjectMapperFactory;

@TestConfiguration
public class TestBeanConfiguration {

	@Bean
	ObjectMapper objectMapper() {
		return ObjectMapperFactory.buildStrictGenericObjectMapper();
	}
}
